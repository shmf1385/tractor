Any form of contributing is appreciated; Inclunding but not limited to:

* Packaging for more GNU/Linux distributions.
* Packaging for [Homebrew](https://brew.sh) and [Msys2](https://www.msys2.org).
* Adding support for more pluggable transports, e.g. [Snowflake](https://snowflake.torproject.org).
